import Vue from "vue";
import MetaInfo from "vue-meta-info";

import VueScrollactive from "vue-scrollactive";

import AOS from "aos";
import "aos/dist/aos.css";

import App from "./App.vue";

Vue.config.productionTip = false;
Vue.use(MetaInfo);
Vue.use(VueScrollactive);

new Vue({
  render: (h) => h(App),
  created() {
    AOS.init({
      duration: 500,
      easing: "cubic-bezier(.48,.01,.52,.99)",
      anchorPlacement: "top-center",
      offset: -100,
      once: true,
    });
  },
  mounted() {
    document.dispatchEvent(new Event("render-event"));
  },
}).$mount("#app");
