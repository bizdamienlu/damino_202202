const path = require("path");
const PrerenderSpaPlugin = require("prerender-spa-plugin");
const NAME = require("./package.json").name;

const Renderer = PrerenderSpaPlugin.PuppeteerRenderer;

const isProduction = process.env.NODE_ENV === "production";
const isDefault = process.env.NODE_ENV === "default";
const isRelease = process.env.NODE_ENV === "release";

module.exports = {
  publicPath: isDefault
    ? "/" + NAME + "/"
    : isRelease
    ? "https://bizdamienlu.gitlab.io/" + NAME + "/"
    : "/",
  outputDir: "dist",
  assetsDir: "static",
  productionSourceMap: false,
  configureWebpack: (config) => {
    if (isProduction || isRelease) {
      config.plugins.push(
        new PrerenderSpaPlugin({
          staticDir: path.join(__dirname, "/dist"),
          routes: ["/"],
          renderer: new Renderer({
            renderAfterDocumentEvent: "render-event",
          }),
          args: ["--disable-setuid-sandbox", "--no-sandbox"],
        })
      );
    }
  },
};
